import { BrowserRouter, Routes as GroupRoute, Route } from 'react-router-dom'

import SignIn from '../pages/SignIn';
import SingUp from '../pages/SignUp';
import Home from '../pages/Home';
import Loading from '../pages/Loading';
import PageNotFound from '../pages/PageNotFound';
import Authorization from '../pages/Authorization';

const Routes = () => {
    return (
        <BrowserRouter>
            <GroupRoute>
                <Route path="/" element={<Home />} />
                <Route path='/login' element={<SignIn />} />
                <Route path='/new' element={<SingUp />} />
                <Route path='/loading' element={<Loading />} />
                <Route path='/auth' element={<Authorization />} />
                <Route path='*' element={<PageNotFound />} />
            </GroupRoute>
        </BrowserRouter>
    )
}

export default Routes;