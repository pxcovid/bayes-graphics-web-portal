import React from 'react'
import ReactDOM from 'react-dom'
import Routes from './routes'
import { Container } from './styles'

ReactDOM.render(
  <React.StrictMode>
    <Container>
      <Routes />
    </Container>
  </React.StrictMode>,
  document.getElementById('root')
)
