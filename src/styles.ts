import styled from 'styled-components';

export const Container = styled.div`
    background-color: #F7F7F7;
    margin: 0px;
    padding: 0px;
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
`;