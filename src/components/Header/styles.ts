import styled from 'styled-components';

export const Container = styled.div`
    width: auto;

    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 32px;
`;

export const Title = styled.div`

    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    gap: 8px;

    * {
        padding: 0px;
        margin: 0px;
    }

`;
export const Action = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;

    * {
    margin-left: 8px;
    }
`;