import { Container } from "./styles";
import LogoMarca from '../../assets/logo.svg';

const AppBar = () => {
    return (
        <>
            <Container> 
                <img src={LogoMarca} height="24px" alt="Logo Marca" />
            </Container>
        </>
    )
}

export default AppBar;
