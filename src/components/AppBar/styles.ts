import styled from 'styled-components';

export const Container = styled.div`
    height: 42px;
    width: 100%;

    display: flex;
    align-items: center;
    justify-content: center;
    box-shadow: 0px 1px 10px rgba(0, 0, 0, 0.12);
`;