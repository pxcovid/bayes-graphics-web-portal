import { Container } from "./styles";

const PageNotFound = () => (<Container> <h3> 404 | Pagina Não Encontrada </h3> </Container>)

export default PageNotFound;