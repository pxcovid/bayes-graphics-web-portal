import { Button, Link, Typography, TextField } from '@mui/material';
import LogoMarca from '../../assets/logo.svg';
import { Container, Content, Title } from './styles';


const SignIn = () => {
    return (
        <Container>
            <Content>
                <header>
                    <img src={LogoMarca} height="48px" alt="Logo Marca" />
                </header>
                <main>
                    <Title>
                        <Typography variant="h5" color="#37474F"> Bem-Vindo de Volta !! </Typography>
                        <Typography variant="subtitle2" color="#B0BEC5">Por-favor entre com seu email que iremos enviar um codigo de autorização</Typography>
                    </Title>

                    <TextField label="Email" size="small" fullWidth />
                    <Button variant="contained" disableElevation fullWidth>Entra</Button>
                    <Typography variant="caption" color="#B0BEC5">ou</Typography>
                    <Button variant="outlined" disableElevation fullWidth> Cadastrar </Button>
                </main>
                <footer>
                    <Typography variant="caption" color="#B0BEC5">Plataforma de Ciencias de Dados Aplicados a Trabalhos Acadêmicos </Typography>
                </footer>
            </Content>

        </Container>
    )
}

export default SignIn;
