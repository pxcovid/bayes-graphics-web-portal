import { Button, Link, Typography, TextField } from '@mui/material';
import LogoMarca from '../../assets/logo.svg';
import { Container, Content, Title } from './styles';


const Authorization = () => {
    return (
        <Container>
            <Content>
                <header>
                    <img src={LogoMarca} height="48px" alt="Logo Marca" />
                </header>
                <main>
                    <Title>
                        <Typography variant="h5" color="#37474F"> Insira o codigo de autorização. </Typography>
                        <Typography variant="subtitle2" color="#B0BEC5">Foi enviado o codigo de autorização para o email cadastrado. </Typography>
                    </Title>

                    <TextField label="Codigo de Autorização" size="small" fullWidth />
                    <Button variant="contained" disableElevation fullWidth>ENTRAR</Button>
                    <Button variant="outlined" disableElevation fullWidth> Voltar </Button>
                </main>
                <footer>
                    <Typography variant="caption" color="#B0BEC5">Plataforma de Ciencias de Dados Aplicados a Trabalhos Acadêmicos </Typography>
                </footer>
            </Content>

        </Container>
    )
}

export default Authorization;
