import AppBar from "../../components/AppBar";
import Header from "../../components/Header";
import { Main } from "./styles";

const Home = () => {
    return (
        <>
            <AppBar />
            <Header />

            <Main>
                <h1> Conteudo</h1>
            </Main>
        </>
    )
}

export default Home;