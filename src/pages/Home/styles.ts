import styled from 'styled-components';

export const Main = styled.div`
    width: auto;

    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-inline: 32px;
`;
