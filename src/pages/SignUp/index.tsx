import { Button, Link, Typography, TextField } from '@mui/material';
import LogoMarca from '../../assets/logo.svg';
import { Container, Content, Title } from './styles';


const Login = () => {
    return (
        <Container>
            <Content>
                <header>
                    <img src={LogoMarca} height="48px" alt="Logo Marca" />
                </header>
                <main>
                    <Title>
                        <Typography variant="h5" color="#37474F"> Criar uma nova Conta </Typography>
                        <Typography variant="subtitle2" color="#B0BEC5"> Criando uma conta é possivel salvar o seu trabalho e acessar em outras dispositivos </Typography>
                    </Title>

                    <TextField label="Email" size="small" fullWidth />
                    <TextField label="Nome Completo" size="small" fullWidth />
                    <TextField label="Usuário" size="small" fullWidth />


                    <Button variant="contained" disableElevation fullWidth>Criar conta</Button>
                    <Button variant="outlined" disableElevation fullWidth> voltar </Button>
                </main>
                <footer>
                    <Typography variant="caption" color="#B0BEC5">Plataforma de Ciencias de Dados Aplicados a Trabalhos Acadêmicos </Typography>
                </footer>
            </Content>

        </Container>
    )
}

export default Login;
